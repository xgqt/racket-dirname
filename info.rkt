#lang info


(define pkg-authors '(xgqt))

(define pkg-desc "Basename and dirname functions")

(define version "0.0")

(define license 'CC0-1.0)

(define collection "dirname")

(define scribblings
  '(("scribblings/dirname.scrbl" (multi-page) (library) "dirname")))

(define deps
  '("base"))

(define build-deps
  '("racket-doc"
    "rackunit-lib"
    "scribble-lib"))
